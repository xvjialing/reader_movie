var network=require("../../data/data.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // date:"Nov 25 2016",
    // title:"正是虾肥蟹壮时"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    network.getArticleList(function(res){
      // console.log(res)
      that.setData({
        post_content:res
      })
    })
  },

  detail : function(event){
    var id=event.currentTarget.dataset.id;
    
    wx.navigateTo({
      url: './post-detail/post-detail?id='+id,
    })
  }
})