var network=require('../../../data/data.js');
var app=getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // isplayingmusic:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取上一页面传递过来的id
    var id=options.id;
    var that=this
    var url = "/api/v1/article/"+id
    network.getArticle(url, function(res){
      // console.log(res)
      //在onload方法中，不是异步地去执行一个数据绑定
      //则不需要使用this.setData()
      //只需要对this.data赋值即可实现数据绑定，比如this.data=postData
      that.setData({
        article:res
      })
    })

    //设置本地缓存
    // wx.setStorageSync("key", "测试缓存数据")

    var status = wx.getStorageSync('collection')
    if (status){
      this.setData({
        collection: status
      })
    }else{
      wx.setStorageSync('collection', false)
    }

    
    
    if (app.globalData.g_isplayingmusic){
      this.setData({
        isplayingmusic:true
      })
    }
    this.setMusicMonitor();
  },

  setMusicMonitor:function(){
    var that=this;
    wx.onBackgroundAudioPlay(function () {
      that.setData({
        isplayingmusic: true
      })
      app.globalData.g_isplayingmusic=true
      app.globalData.g_currentMusicpostId = that.data.article.music.id;
    })
    wx.onBackgroundAudioPause(function () {
      that.setData({
        isplayingmusic: false
      })
      app.globalData.g_isplayingmusic = false
      app.globalData.g_currentMusicpostId =null
    })
  },

  onUnload:function(){
    // console.log("onUnload")
    this.setData({
      isplayingmusic:false
    })
  },

  onCollectionTap: function(){
    var status=wx.getStorageSync('collection')
    var postcollection = !status;
    
    wx.setStorageSync('collection', postcollection)
    this.setData({
      collection: postcollection
    })
    // if(postcollection){
    //   wx.showToast({
    //     title: '收藏成功',
    //     icon: 'success',
    //     duration: 2000
    //   })
    // }else{
    //   wx.showToast({
    //     title: '取消成功',
    //     duration: 2000
    //   })
    // }

    wx.showModal({
      title: '收藏',
      content: '是否收藏文章',
      showCancel: true,
      cancelText: '取消',
      confirmText: '确定'
    })
  },

  onShareTap: function(event){
    // wx.removeStorageSync('key')
    // wx.clearStorageSync()

    wx.showActionSheet({
      itemList: [
        '分享给微信好友',
        '分享到朋友圈',
        '分享到qq',
        '分享到微博'
      ],
      itemColor: "#405f80",
      success: function(res){
        //res.tapIndex 数组元素的序号，从0开始
        console.log(res.tapIndex)

      },
      fail:function(res){
        //res.cancle() 用户是否点击了取消按钮
        console.log(res.errMsg)
      }
    })
  },

  onMusicTap:function(){
    // console.log(this.data.article.music)
    var music = this.data.article.music;
    var isplaying = this.data.isplayingmusic;
    var that=this;
    if (isplaying){
      wx.pauseBackgroundAudio()
      // this.data.isplayingmusic=false;
      this.setData({
        isplayingmusic:false
      })
    }else{
      wx.playBackgroundAudio({
        dataUrl: music.url,
        title: music.title,
        coverImgUrl: music.coverImg,
        success: function(res){
          // console.log(res.status)
          // that.data.isplayingmusic = true;
          that.setData({
            isplayingmusic: true
          })
        }
      })
    }
    
  }
})