var network = require("../../data/data.js") 
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inTheaters:{},
    comingSoon:{},
    top250:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    this.getInTheaters(3);
    this.getComing_soon(3);
    this.getTop250(3);

  },

  getInTheaters:function(count){
    var that = this;
    network.getInTheaters(count, function (res) {
      // console.log(res)
      var movies = res.data.subjects;
      for (var idx in movies) {
        var movie = movies[idx]
        var title = movie.title
        if (title.length > 6) {
          movies[idx].title = title.substring(0, 6) + "...";
        }
      }
      // console.log(movies)
      var movieData = {};
      movieData["inTheaters"] = {
        movies: movies,
        tag: res.data.title,
        url: "/v2/movie/in_theaters"
      };
      that.setData(movieData)
    })
  },

  getComing_soon:function(count){
    var that=this;
    network.getComing_soon(count, function (res) {
      var movies = res.data.subjects;
      for (var idx in movies) {
        var movie = movies[idx]
        var title = movie.title
        if (title.length > 6) {
          movies[idx].title = title.substring(0, 6) + "...";
        }
      }
      // console.log(movies)
      var movieData = {};
      movieData["comingSoon"] = {
        movies: movies,
        tag: res.data.title,
        url: "/v2/movie/coming_soon"
      };
      that.setData(movieData)
    })
  },

  getTop250:function(count){
    var that=this;

    network.getTop250(count, function (res) {
      var movies = res.data.subjects;
      for (var idx in movies) {
        var movie = movies[idx]
        var title = movie.title
        if (title.length > 6) {
          movies[idx].title = title.substring(0, 6) + "...";
        }
      }
      // console.log(movies)
      var movieData = {};
      movieData["top250"] = {
        movies: movies,
        tag: res.data.title,
        url: "/v2/movie/top250"
      };
      that.setData(movieData)
    })
  },

  //点击更多按钮
  onMoreTap:function(event){
    var url = event.currentTarget.dataset.url;
    var title = event.currentTarget.dataset.title;
    wx.navigateTo({
      url: './more-movie/more-movie?url='+url+'&title='+title,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})