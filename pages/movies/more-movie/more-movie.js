var network = require("../../../data/data.js")

// pages/movies/more-movie/more-movie.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    movies: [],
    start: 0,
    url: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: options.title,
    })
    this.setData({
      url: options.url
    })
    this.getMovie(options.url, 0, 20)
  },

  getMovie: function (url, start, count) {
    var that = this;
    var movies=this.data.movies
    network.getMovie(url, start, count, function (res) {
      // console.log(res)
      var recent_movies = res.data.subjects;
      for (var idx in recent_movies) {
        var movie = recent_movies[idx]
        var title = movie.title
        if (title.length > 6) {
          movie.title = title.substring(0, 6) + "...";
        }
        movies.push(movie)
      }
      // movies = lastmovies.push(movies)
      that.setData({
        movies: movies,
        start: start + 20
      })
    })
  },

  lower: function (e) {
    console.log("触底")
    var start = this.data.start;
    var url = this.data.url;
    this.getMovie(url, start, 20)
  },

  upper: function(e){
    console.log("触顶")
    this.setData({
      movies: [],
      start: 0,
    })
    var url = this.data.url;
    this.getMovie(url, 0, 20)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})