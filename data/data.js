var host = 'http://www.xvjialing.xyz:8081'
// var host = 'http://192.168.0.114:8081'
// var douban_host ="https://api.douban.com"
var douban_host = "https://www.langyangtech.com"

function getArticleList(success) {
  wx.request({
    url: host + '/api/v1/article',
    data: {
      page: 0
    },
    method: 'GET',
    success: function (res) {
      // var post_content= res.data.data.content;
      // console.log(post_content);

      success(res.data.data.content);
    }
  })
}

function getArticle(url, success) {
  wx.request({
    url: host + url,
    method: 'GET',
    success: function (res) {
      // var post_content= res.data.data.content;
      // console.log(post_content);

      success(res.data.data);
    }
  })
}

//即将上映
function getComing_soon(count, success) {
  wx.request({
    url: douban_host + '/v2/movie/coming_soon' + "?start=0&&count=" + count,
    method: 'GET',
    header: {
      "Content-Type": "application/xml"
    },
    success: function (res) {
      // success(res.data.subjects)
      success(res)
    }
  })
}

// 获取正在热映电影
function getInTheaters(count, success) {
  wx.request({
    url: douban_host + '/v2/movie/in_theaters' + "?start=0&&count=" + count,
    method: 'GET',
    header: {
      "Content-Type": "application/xml"
    },
    success: function (res) {
      // console.log(res);
      // success(res.data.subjects);
      success(res)
    }
  })
}

//获取top250
function getTop250(count, success) {
  wx.request({
    url: douban_host + '/v2/movie/top250' + "?start=0&&count=" + count,
    method: 'GET',
    header: {
      "Content-Type": "application/xml"
    },
    success: function (res) {
      // success(res.data.subjects);
      success(res)
    }
  })
}

function getMovie(url, start, count, success) {
  wx.request({
    url: douban_host + url + "?start=" + start + "&&count=" + count,
    method: 'GET',
    header: {
      "Content-Type": "application/xml"
    },
    success: function (res) {
      // success(res.data.subjects);
      success(res)
    }
  })
}

module.exports = {
  getArticleList: getArticleList,
  getArticle: getArticle,
  getInTheaters: getInTheaters,
  getComing_soon: getComing_soon,
  getTop250: getTop250,
  getMovie: getMovie
}
